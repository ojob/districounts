%rebase('base.tpl')
%if result['balance']:
<p>Here's the list of flows to balance {{ project['name'] }}:</p>
<ul>
    %for sender, receiver, balance_flow in result['balance']:
    <li>{{sender}}→{{receiver}}: {{balance_flow}}
        <form action="/projects/{{ project['slug'] }}/records" method="post">
            <input name="action" value="create" hidden="true" />
            <input name="sender" type="text" value="{{ sender }}" hidden="true" />
            <input name="receivers" type="text" value="{{ receiver }}" hidden="true" />
            <input name="amount" type="text" value="{{ balance_flow }}" hidden="true" />
            <input value="Track refunding" type="submit" />
        </form>
    </li>
    %end
</ul>
%else:
<p>No flows to balance this instance.</p>
%end
