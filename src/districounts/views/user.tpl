%rebase('base.tpl')
%if defined('result') and result['users']:
%    user = result['users'][0]
<form action="/projects/{{ project['slug'] }}/users/uid={{ user['uid'] }}" method="post">
    <input name="action" value="update" hidden="true" />
    <input name="uid" value="{{ user['uid'] }}" hidden="true" />
    Name: <input name="name" type="text" value="{{ user['name'] }}"/>
    Email: <input name="email" type="text" value="{{ user['email'] }}"/>
    <input value="update" type="submit" />
</form>
%else:
<p>No user to display!</p>
%end
