%rebase('base.tpl')
%if result['records']:
<p>Here's the list of records in this instance:</p>
<ul>
    %for record in result['records']:
    <li><a href="/projects/{{ project['slug'] }}/records/rid={{ record['rid'] }}">{{ record['rid'] }}</a>&nbsp;:
        {{ record['sender'] }}&nbsp;→&nbsp;{{ ', '.join(str(rec) for rec in sorted(record['receivers'])) }}&nbsp;:
        {{ record['amount'] }}{{ record['currency'] }}
        <form action="/projects/{{ project['slug'] }}/records" method="post">
            <input name="action" value="delete" hidden="true" />
            <input name="rid" value="{{ record['rid'] }}" hidden="true" />
            <input value="X" type="submit" />
        </form>
    </li>
    %end
</ul>
%else:
<p>No records in this instance.</p>
%end
<form action="/projects/{{ project['slug'] }}/records" method="post">
    <input name="action" value="create" hidden="true" />
    Sender: <input name="sender" type="text" />
    Receivers: <input name="receivers" type="text" />
    Amount: <input name="amount" type="text" />
    <input value="create" type="submit" />
</form>
