# coding: utf-8
"""This module provides a front-end using Bottle templating system.

This is heavily based on :py:class:`backend.Api`, even using directly its
methods.

"""

# ---- build-in imports ---------------------------------------------------
import typing
from decimal import Decimal

# ---- third-party imports ------------------------------------------------
import bottle

# ---- local imports ------------------------------------------------------
from . import LINKS, __author__, backend, models, utils


# ---- classes definition -------------------------------------------------
class HtmlFrontend(backend.BottleApp):
    """Create HTML front-end, based on :py:class:`Api`.

    Once instantiated, merge this app with the Api.

    Web HTML: for interaction through a web browser.

    Following addresses are curently working:

    - `/` or `/home`: home page of the app instance
    - '/projects': list of projects in the app instance
    - '/projects/<slug>': home page of a project
    - `/projects/<slug>/users`: shows users in the project
    - `/projects/<slug>/users/uid=<uid>`: show user with id=`<uid>`
    - `/projects/<slug>/users/balance`: shows balance for each user in the
      project
    - `/projects/<slug>/records`: shows transactions in the project
    - `/projects/<slug>/records/rid=<rid>`: show record with id=`<rid>`
    - `/projects/<slug>/balance`: shows balance for the project

    """

    def __init__(self, api_app: backend.Api, *args, **kwargs):
        """Initialize."""
        super().__init__(*args, **kwargs)
        self.api = api_app
        # add some links, for backend module decorators to work
        self.metadata = self.api.metadata
        self.projects = self.api.projects
        self.error_404 = self.api.error_404

    # ---- HTML entry points ----------------------------------------------
    @backend.route(path='/', verb='GET')
    @backend.route(path='/home', verb='GET')
    @bottle.view('home')
    @backend.add_metadata
    @utils.set_res(title='Districounts Home')
    def web_home(
            self,
    ) -> typing.Dict[str, typing.Any]:
        """Show homepage."""
        nb_projects = len(self.api.list_projects()['result']['projects'])
        return {'author': __author__,
                'nb_projects': nb_projects,
                'links': LINKS}

    # no need to decorate with `app_version`, as this is already part of
    # return value of the API.

    # ---- projects routes ----
    @backend.route(path='/projects', verb='GET')
    @bottle.view('projects')
    @utils.set_res(title='Districounts Projects')
    def web_list_projects(
            self,
            **kwargs,
    ) -> typing.Dict[str, typing.Any]:
        """Show list of projects.

        Args:
            kwargs: items to be added to template.

        """
        res = self.api.list_projects()
        res.update(kwargs)
        return res

    # pylint: disable=no-self-use  # necessary for @backend.add_metadata call
    @backend.route(path='/projects/<slug>', verb='GET')
    @bottle.view('project')
    @backend.get_project_from_slug(failure_effect='err-404')
    @backend.add_metadata
    def web_show_project(
            self,
            project: models.SharesProject,
            **kwargs,
    ) -> typing.Dict[str, typing.Any]:
        """Show *project* data.

        Args:
            kwargs: items to be added to template.

        """
        res = {'project': project.as_dict(),
               'nb_users': len(project.users),
               'nb_records': len(project.records)}
        res.update(kwargs)
        return res

    @backend.route(path='/projects', verb='POST')
    @utils.sanitize(request_attr='forms',
                    required_params=['action'],
                    requested_params=['slug', 'name'],
                    failure_effect='pass-msg')
    def web_create_or_delete_project(
            self,
            action: str = None,
            slug: models.SlugLike = None,
            name: str = None,
            msg: str = None,
    ) -> typing.Dict[str, typing.Any]:
        """Handle creation or deletion of a project."""
        if msg is not None:
            msg_type = 'error'
        elif action == 'create':
            try:
                self.api.projects.create_project(name=name)
            except models.ProjectCreateError as exc:
                msg = f"Cannot create project: {exc}"
                msg_type = 'error'
            else:
                msg, msg_type = 'Project created', 'notice'
        elif action == 'delete':
            project = self.api.projects.delete_project(slug=slug)
            msg, msg_type = f'Project {project.name} deleted', 'notice'
        else:
            msg, msg_type = f'unknown action: {action}', 'warning'

        return self.web_list_projects(msg=msg, msg_type=msg_type)

    @backend.route(path='/projects/<slug>', verb='POST')
    @backend.get_project_from_slug(failure_effect='err-404')
    @utils.sanitize(request_attr='forms',
                    required_params=['action'],
                    requested_params=['name'],
                    failure_effect='pass-msg')
    def web_update_project(
            self,
            project: models.SharesProject,
            action: str = None,
            name: str = None,
            msg: str = None,
    ) -> typing.Dict[str, typing.Any]:
        """Handle update of project."""
        if msg is not None:
            msg_type = 'error'
        else:
            if action == 'update':
                self.api.projects.update_project(
                    slug=project.slug, new_name=name)
                msg, msg_type = 'Project updated', 'notice'
            else:
                msg, msg_type = f'unknown action: {action}', 'warning'
        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        return self.web_show_project(slug=project.slug,
                                     msg=msg, msg_type=msg_type)

    # ---- users routes ----
    @backend.route(path='/projects/<slug>/users', verb='GET')
    @backend.get_project_from_slug(failure_effect='err-404')
    @bottle.view('users')
    @backend.add_metadata
    def web_list_users(
            self,
            project: models.SharesProject,
            **kwargs,
    ) -> typing.Dict[str, typing.Any]:
        """Show list of users for *project*.

        Args:
            kwargs: items to be added to template.

        """
        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        res = self.api.list_users(slug=project.slug)
        res.update(dict(title=f"Districounts {project.name} users",
                        project=project.as_dict()))
        res.update(kwargs)
        return res

    @backend.route(path='/projects/<slug>/users/uid=<uid_raw:int>', verb='GET')
    @backend.get_project_from_slug(failure_effect='err-404')
    @bottle.view('user')
    @utils.set_res(title='Districounts Users')
    def web_show_user(
            self,
            project: models.SharesProject,
            uid_raw: int,
            **kwargs
    ) -> typing.Dict[str, typing.Any]:
        """Show list of users."""
        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        res = self.api.show_user(slug=project.slug, uid_raw=uid_raw)
        res.update(dict(title=f"Districounts {project.name} user details",
                        project=project.as_dict()))
        res.update(kwargs)
        return res

    @backend.route(path='/projects/<slug>/users', verb='POST')
    @backend.get_project_from_slug(failure_effect='err-404')
    @utils.sanitize(request_attr='forms',
                    required_params=['action', 'name'],
                    requested_params=['uid', 'email'],
                    converters=backend.USER_CONVERTERS,
                    failure_effect='pass-msg')
    def web_create_or_delete_user(
            self,
            project: models.SharesProject,
            action: str = None,
            name: str = None,
            uid: models.UserId = None,
            email: str = None,
            msg: str = None,
    ) -> typing.Dict[str, typing.Any]:
        """Handle creation or deletion of a user."""
        if msg is not None:
            msg_type = 'error'
        elif action == 'create':
            try:
                project.create_user(name=name, email=email)
            except models.UserCreateError as exc:
                msg = f"Cannot create user: {exc}"
                msg_type = 'error'
            else:
                msg, msg_type = 'User created', 'notice'
        elif action == 'delete':
            try:
                project.delete_user(uid)
            except models.UserDeleteError as exc:
                msg = f"Cannot delete user: {exc}"
                msg_type = 'error'
            else:
                msg, msg_type = f'User {name} deleted', 'notice'
        else:
            msg, msg_type = f'unknown action: {action}', 'warning'

        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        return self.web_list_users(slug=project.slug,
                                   msg=msg, msg_type=msg_type)

    @backend.route(path='/projects/<slug>/users/uid=<uid_raw:int>',
                   verb='POST')
    @backend.get_project_from_slug(failure_effect='err-404')
    @utils.sanitize(request_attr='forms',
                    required_params=['action'],
                    requested_params=['uid', 'name', 'email'],
                    converters=backend.USER_CONVERTERS,
                    failure_effect='pass-msg')
    def web_update_user(
            self,
            project: models.SharesProject,
            uid_raw: int = None,
            action: str = None,
            uid: models.UserId = None,
            name: str = None,
            email: str = None,
            msg: str = None,
    ) -> typing.Dict[str, typing.Any]:
        """Handle update of user."""
        if msg is not None:
            msg_type = 'error'
        else:
            if str(uid_raw) != str(uid):
                raise ValueError("inconsistent request")
            if action == 'update':
                project.update_user(uid=uid, name=name, email=email)
                msg, msg_type = 'User updated', 'notice'
            else:
                msg, msg_type = f'unknown action: {action}', 'warning'
        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        return self.web_show_user(slug=project.slug, uid_raw=uid_raw,
                                  msg=msg, msg_type=msg_type)

    # ---- records routes ----
    @backend.route(path='/projects/<slug>/records', verb='GET')
    @backend.get_project_from_slug(failure_effect='err-404')
    @bottle.view('records')
    @utils.set_res(title='Districounts Records')
    def web_list_records(
            self,
            project: models.SharesProject,
            **kwargs,
    ) -> typing.Dict[str, typing.Any]:
        """Show list of records.

        Args:
            kwargs: items to be added to template.

        """
        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        res = self.api.list_records(slug=project.slug)
        res.update(project=project.as_dict())
        res.update(kwargs)
        return res

    @backend.route(path='/projects/<slug>/records/rid=<rid_raw>', verb='GET')
    @backend.get_project_from_slug(failure_effect='err-404')
    @bottle.view('record')
    @utils.set_res(title='Districounts Records')
    def web_show_record(
            self,
            project: models.SharesProject,
            rid_raw: str,
            **kwargs,
    ) -> typing.Dict[str, typing.Any]:
        """Show given record.

        Args:
            kwargs: items to be added to template.

        """
        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        res = self.api.show_record(slug=project.slug, rid_raw=rid_raw)
        res.update(dict(title=f"Districounts {project.name} user details",
                        project=project.as_dict()))
        res.update(kwargs)
        return res

    @backend.route(path='/projects/<slug>/records', verb='POST')
    @backend.get_project_from_slug(failure_effect='err-404')
    @utils.sanitize(request_attr='forms',
                    required_params=['action'],
                    requested_params=['sender', 'receivers', 'amount', 'rid'],
                    converters=backend.WEB_RECORD_CONVERTERS,
                    failure_effect='pass-msg')
    def web_create_or_delete_record(
            self,
            project: models.SharesProject,
            action: str = None,
            sender: models.UserId = None,
            receivers: typing.List[models.UserId] = None,
            amount: Decimal = None,
            rid: models.RecordId = None,
            msg: str = None,
    ) -> typing.Dict[str, typing.Any]:
        """Handle creation or deletion of a record."""
        if msg is not None:
            msg_type = 'error'
        elif action == 'create':
            try:
                # No protection here against values/types, this is performed in
                # :py:mod:`models`.
                project.create_record(
                    sender=sender, receivers=receivers, amount=amount)
            except models.RecordCreateError as exc:
                msg = f"Cannot create record: {exc}"
                msg_type = 'error'
            else:
                msg, msg_type = 'Record created', 'notice'
        elif action == 'delete':
            project.delete_record(rid)
            msg, msg_type = f'Record {rid} deleted', 'notice'
        else:
            msg, msg_type = f'unknown action: {action}', 'warning'

        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        return self.web_list_records(slug=project.slug,
                                     msg=msg, msg_type=msg_type)

    @backend.route(path='/projects/<slug>/records/rid=<rid_raw>', verb='POST')
    @backend.get_project_from_slug(failure_effect='err-404')
    @utils.sanitize(request_attr='forms',
                    required_params=['action'],
                    requested_params=['rid', 'sender', 'receivers', 'amount'],
                    converters=backend.WEB_RECORD_CONVERTERS,
                    failure_effect='pass-msg')
    def web_update_record(
            self,
            project: models.SharesProject,
            rid_raw: str = None,
            action: str = None,
            rid: models.RecordId = None,
            sender: models.UserId = None,
            receivers: typing.List[models.UserId] = None,
            amount: Decimal = None,
            msg: str = None,
    ) -> typing.Dict[str, typing.Any]:
        """Handle creation or deletion of a record."""
        if msg is not None:
            msg_type = 'error'
        else:
            if rid_raw != str(rid):
                raise ValueError("inconsistent request, rid in URL different "
                                 "from payload")
            rid = models.RecordId(rid_raw)
            if action == 'update':
                project.update_record(
                    rid=rid, sender=sender, receivers=receivers, amount=amount)
                msg, msg_type = 'Record updated', 'notice'
            else:
                msg, msg_type = f'unknown action: {action}', 'warning'
        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        return self.web_show_record(slug=project.slug, rid_raw=rid_raw,
                                    msg=msg, msg_type=msg_type)

    @backend.route(path='/projects/<slug>/users/balance', verb='GET')
    @backend.get_project_from_slug(failure_effect='err-404')
    @bottle.view('users_balance')
    @utils.set_res(title='Districounts Users Balance')
    def web_list_users_balance(
            self,
            project: models.SharesProject,
    ) -> typing.Dict[str, typing.Any]:
        """Show users' balance."""
        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        res = self.api.users_balance(slug=project.slug)
        res.update(project=project.as_dict())
        return res

    @backend.route(path='/projects/<slug>/balance', verb='GET')
    @backend.get_project_from_slug(failure_effect='pass-msg')
    @bottle.view('balance')
    @utils.set_res(title='Districounts Project Balance')
    def web_balance(
            self,
            project: models.SharesProject,
    ) -> typing.Dict[str, typing.Any]:
        """Show project balance."""
        # due to get_project_from_slug decorator:
        # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
        res = self.api.project_balance(slug=project.slug)
        res.update(project=project.as_dict())
        return res
