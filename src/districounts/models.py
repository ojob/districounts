# coding: utf-8
"""This module provides the data model of districounts."""

# ---- build-in imports ---------------------------------------------------
from __future__ import annotations

import collections
import copy
import dataclasses
import logging
import random  # for sample data creation
import typing
import uuid
from decimal import Decimal, InvalidOperation

# ---- third-party imports ------------------------------------------------
from slugify import slugify

# ---- local imports ------------------------------------------------------
from . import utils

# ---- constants definition -----------------------------------------------


# ----custom types, validators and context definition ---------------------

# ---- exceptions definition ----------------------------------------------
class UserCreateError(Exception):
    """Exception for :py:class:`User` creation."""


class UserDeleteError(Exception):
    """Exception for :py:class:`User` delete."""


class RecordCreateError(Exception):
    """Exception for :py:class:`Record` creation."""


class RecordAccessError(Exception):
    """Exception for access to :py:class:`Record` creation."""


class RecordUpdateError(Exception):
    """Exception for :py:class:`Record` update."""


class RecordDeleteError(Exception):
    """Exception for :py:class:`Record` delete."""


class RecordInconsistency(Exception):
    """Inconsistency between :py:class:`Record` and :py:class:`RecordStore`.

    This can be raised at storage or update.
    """


class ProjectCreateError(Exception):
    """Exception for :py:class:`SharesProject` creation."""


class ProjectAccessError(Exception):
    """Exception for :py:class:`SharesProject` access."""


class ProjectUpdateError(Exception):
    """Exception for :py:class:`SharesProject` update."""


class ComputationError(Exception):
    """Exception to track computation errors."""


class LoadError(Exception):
    """Exception to track loading errors."""


# ---- functions definition -----------------------------------------------


# ---- classes definition -------------------------------------------------
class UserId(int):
    """Class to handle user id, including creation.

    >>> uid = UserId(uid=13)
    >>> uid
    UserId(uid=13)
    >>> str(uid)
    '13'

    This inherits from `int`, and therefore benefits from some of its
    useful methods, including casting from floats and strings:

    >>> uid == 13
    True
    >>> hash(uid)
    13
    >>> UserId(uid='13')
    UserId(uid=13)
    >>> UserId(uid=13.0)
    UserId(uid=13)

    This means in particular that `UserId` can be used as a dictionary key.

    Input data shall however be castable to int.

    >>> UserId('13/12')
    Traceback (most recent call last):
    ...
    ValueError: invalid literal for int() with base 10: '13/12'
    >>> UserId('one')
    Traceback (most recent call last):
    ...
    ValueError: invalid literal for int() with base 10: 'one'


    """

    _last_max_uid: int = -1  #: track last maximum used id, as int

    def __new__(cls, uid: UserIdLike = None) -> UserId:
        """Create new immutable instance.

        Args:
            uid: initial ID value. If not provided, a new value is generated.

        """
        if uid is None:
            uid = UserId._last_max_uid + 1
        else:
            uid = int(uid)
        UserId._last_max_uid = max(uid, UserId._last_max_uid)
        return int.__new__(cls, uid)

    def __repr__(self) -> str:
        """Represent."""
        return f"UserId(uid={self})"


# pylint: disable=too-few-public-methods
@dataclasses.dataclass
class User:
    """Class to define a user; *mail* is optional.

    The ``uid`` attribute is managed so as to be unique, and is therefore
    incremented

    >>> user = User(name='bob'); user
    User(uid=..., name='bob', email=None)
    >>> user.as_dict()
    {'uid': ..., 'name': 'bob', 'email': None}

    >>> User(name='joe', uid=132).uid
    UserId(uid=132)

    >>> User.from_dict(dict(name='dcosdb')).name
    'dcosdb'
    >>> User.from_dict(dict(uid='19874', name='Simone')).uid
    UserId(uid=19874)

    """

    name: str
    email: str = None
    uid: dataclasses.InitVar[UserIdLike] = None  # for __post_init__

    def __post_init__(self, uid: UserIdLike = None):
        """Initialize."""
        self.uid: UserId = UserId(uid=uid)
        self.name = str(self.name)
        if not self.name:
            raise UserCreateError('missing: name')

    def __repr__(self) -> str:
        """Represent."""
        email_str = 'None' if self.email is None else f"'{self.email}'"
        return (f"{self.__class__.__qualname__}"
                f"(uid={self.uid}, name='{self.name}', email={email_str})")

    @classmethod
    def from_dict(
            cls,
            data: typing.Dict[str, typing.Any],
    ) -> User:
        """Create new user from dictionary.

        The interest here is that some keys may be too much; they will
        simply be ignored.

        """
        return utils.dataclass_from_dict(cls, data, requested=['uid'],
                                         exc_to_raise=UserCreateError)

    def as_dict(self) -> dict:
        """Export as dictionary, using JSON-compatible types."""
        return {'uid': int(self.uid), 'name': self.name, 'email': self.email}


class UserStore(dict):
    """Class to store users, accessible by `uid` or by `name`.

    Use :py:meth:`create` to create a new user and add it to the store.

    """

    def __init__(self, json_data: dict = None):
        """Initialize."""
        super().__init__()
        self.logger = logging.getLogger(self.__class__.__qualname__)
        self.by_name: typing.Dict[typing.Text, typing.Set[UserId]] = \
            collections.defaultdict(set)
        if json_data is not None:
            self.load(json_data=json_data)

    def create_sample(self, nb_entries=5):
        """Create some sample data."""
        sample_names = ('Bob', 'Sarah', 'Joe', 'Noria', 'Élisabette')
        names = sample_names * (1 + nb_entries // len(sample_names))
        for name in names[:nb_entries]:
            self.create(name=name, email=f'{name}@{name}.net')

    def add(self, user: User) -> None:
        """Add an existing user.

        Raises:
            ValueError: if *user* id is already stored.

        """
        if user.uid in self:
            raise ValueError(f"user id already stored: {user.uid}")
        self[user.uid] = user
        self.by_name[user.name].add(user.uid)

    def load(
            self,
            json_data: dict,
    ) -> str:
        """Load content of *json_data* and add it to the store.

        Args:
            json_data: dict of data from a JSON input file.

        """
        prev_nb = len(self)
        for user_dict in json_data['users']:
            user = User.from_dict(user_dict)
            self[user.uid] = user
        new_nb = len(self) - prev_nb
        mod_nb = len(self) - new_nb
        return f"users: {new_nb} new, {mod_nb} mod"

    def dump(self) -> dict:
        """Dump content to a dict, ready for JSON dumping to a file."""
        users = []
        for user in self.values():
            users.append(user.as_dict())
        return {'users': users}

    # ---- CRUD methods ---------------------------------------------------
    def create(
            self,
            name: typing.Text,
            email: typing.Text = None,
    ) -> UserId:
        """Create a new user by properties.

        Args:
            name: name of the user to create; mandatory.
            email: email of the user; optional.

        Returns:
            ID of the new user.

        """
        new_user = User(name=name, email=email)
        self.add(user=new_user)
        return new_user.uid

    def update(
            self,
            uid: UserId,
            name: typing.Text = None,
            email: typing.Text = None,
    ) -> User:
        """Update a given user.

        Args:
            uid: identifier of the user to update.
            name: new name for the user; optional.
            email: new email for the user; optional.

        Returns:
            User instance.

        """
        uid_sf = UserId(uid)
        user = self[uid_sf]
        if name is not None:
            self.by_name[user.name].remove(uid_sf)
            user.name = name
            self.by_name[user.name].add(uid_sf)
        if email is not None:
            user.email = email
        return user

    def delete(
            self,
            uid: UserIdLike,
    ) -> User:
        """Delete a given user.

        Args:
            uid: identifier of the user to update.

        Returns:
            User instance that is removed.

        """
        uid_sf = UserId(uid)
        try:
            user = self[uid_sf]
        except KeyError as exc:
            raise UserDeleteError(f"no user with uid={uid_sf}") from exc
        del self[uid_sf]
        assert uid_sf in self.by_name[user.name]  # should never happen...
        self.by_name[user.name].remove(uid_sf)
        return user

    # ---- magic methods --------------------------------------------------
    @typing.overload
    def __getitem__(self, key: UserIdLike) -> User:
        """Return user by uid."""
    @typing.overload
    def __getitem__(self, key: typing.Text) -> typing.List[User]:
        """Return list of users maching *name*."""

    def __getitem__(self, key):
        """Return one or more users, depending of key type."""
        if isinstance(key, (int, UserId)):
            return super().__getitem__(key)
        # by name, taking care that self.by_name is a defaultdict so will
        # never fail to get querried by key
        uids: typing.List[UserId] = self.by_name[key]
        if not uids:
            raise KeyError(f"unknown entry: {key}")
        return [self[uid] for uid in uids]

    def __iter__(self) -> typing.Iterator[User]:
        """Iterate over the users."""
        for user in self.values():
            yield user

    def __repr__(self) -> str:
        """Represent."""
        if not self:
            user_str = 'no user'
        elif len(self) == 1:
            user_str = '1 user'
        else:
            user_str = f"{len(self)} users"
        return f"{self.__class__.__qualname__}({user_str})"


class RecordId(uuid.UUID):
    """Class to handle record ids, including creation.

    >>> some_uuid_str = '2c1126f1-5625-45f5-a4bd-20028fca79b0'
    >>> rid = RecordId(some_uuid_str)
    >>> rid
    RecordId(rid=2c1126f1-5625-45f5-a4bd-20028fca79b0)
    >>> str(rid) == some_uuid_str
    True
    >>> rid == uuid.UUID(some_uuid_str)
    True
    >>> rid_2 = RecordId()
    >>> rid_2 != rid
    True

    Two `RecordId` instances created from the same UUID string compare equal,
    even if they are not the same object.

    >>> rid_new = RecordId(some_uuid_str)
    >>> rid_new is rid
    False
    >>> rid_new == rid
    True

    """

    def __init__(self, rid: RecordIdLike = None) -> None:
        """Create new immutable instance.

        Args:
            rid: initial ID value. If not provided, a new value is generated.

        """
        if rid is None:
            rid = uuid.uuid4()
        super().__init__(str(rid))  # convert to str to ensure initialization

    def __repr__(self) -> str:
        """Represent."""
        return f"RecordId(rid={self})"


@dataclasses.dataclass
class Record:
    """Class to store a transation record.

    `amount` is stored internally as :py:class:`decimal.Decimal`. It can be
    passed as :py:class:`AmountInputType`, i.e. as `int`, `float` or
    :py:class:`decimal.Decimal`. Using

    >>> rec = Record(sender=1, receivers=[1, 2], amount=1.2)
    >>> str(rec.as_dict())[:74]
    "{'rid': ..., 'sender': 1, 'receivers': "
    >>> str(rec.as_dict())[74:]
    "[1, 2], 'amount': 1.2, 'currency': '€'}"

    It can also be created from a dictionary:

    >>> rec_2 = Record.from_dict(dict(sender=1, receivers=[1, 2], amount=1.2,
    ...                               some_other_entry='bolb'))
    >>> str(rec_2.as_dict())[:74]
    "{'rid': ..., 'sender': 1, 'receivers': "
    >>> str(rec_2.as_dict())[74:]
    "[1, 2], 'amount': 1.2, 'currency': '€'}"

    >>> Record.from_dict(dict(sender=0))
    Traceback (most recent call last):
    ...
    districounts.models.RecordCreateError: field(s) missing: amount, receivers

    Record ID can be defined at creation:

    >>> rid = RecordId()
    >>> rec = Record(rid=rid, sender=1, receivers=[1], amount=1.0)
    >>> rec.rid == rid
    True

    """

    sender: UserIdLike
    receivers: typing.List[UserIdLike]
    amount: AmountLike
    rid: dataclasses.InitVar[RecordIdLike] = None  # for __post_init__
    currency: typing.Text = '€'

    def __post_init__(self, rid: RecordIdLike = None):
        """Ensure data types."""
        self.rid: RecordId = RecordId(rid=rid)
        self.amount = utils.fmt_amount(self.amount)

    @classmethod
    def from_dict(
            cls,
            data: typing.Dict[str, typing.Any],
    ) -> None:
        """Create new record from dictionary.

        The interest here is that some keys may be too much; they will
        simply be ignored.

        """
        return utils.dataclass_from_dict(cls, data, requested=['rid'],
                                         exc_to_raise=RecordCreateError)

    def as_dict(self) -> typing.Dict[str, typing.Any]:
        """Export as dictionary, using JSON-compatible types."""
        return {'rid': str(self.rid),
                'sender': self.sender, 'receivers': self.receivers,
                'amount': float(self.amount), 'currency': self.currency}


class RecordStore(dict):
    """Class to store counts.

    Use :py:meth:`create` to create a new transaction record, and add it to
    the listed records.

    Senders and Receveirs are identified by their id's.

    """

    def __init__(self, users: UserStore, json_data: dict = None):
        """Initialize.

        Args:
            users: instance of users storage, used for attribution of records
                to users.
            json_dict: if provided, JSON file content to be iniliazed from.

        """
        super().__init__()
        #: list of transaction records by sender
        self.senders: typing.Dict[UserId, typing.Set[RecordId]] = \
            collections.defaultdict(set)
        #: members
        self.users: typing.Dict[UserId, User] = users

        if json_data is not None:
            self.load(json_data=json_data)

    # ---- overall content management methods -----------------------------
    def create_sample(self, nb_entries: int = 3):
        """Create some sample data."""
        user_ids = list(self.users.keys())
        if len(user_ids) < nb_entries:
            user_ids *= (1 + nb_entries // len(user_ids))
        for user_uid in user_ids[:nb_entries]:
            self.create(
                sender=user_uid,
                receivers=random.sample(user_ids, 3),
                amount=utils.fmt_amount(random.randint(1, 100)))

    def load(
            self,
            json_data: dict,
    ) -> str:
        """Load *json_data* (as dict) and add it to the store.

        Args:
            json_data: data from JSON file to be loaded.

        """
        prev_nb = len(self)
        for record_data in json_data['records']:
            # This will do the consistency checks
            try:
                self.add(record=Record.from_dict(record_data))
            except RecordInconsistency as exc:
                raise LoadError(f"load failed: {exc}") from exc
        new_nb = len(self) - prev_nb
        mod_nb = len(self) - new_nb
        return f"records: {new_nb} new, {mod_nb} mod"

    def dump(self) -> dict:
        """Dump content to a dict ready for write in JSON format."""
        return {'records': [record.as_dict() for record in self.values()]}

    # ---- internal methods -----------------------------------------------
    def add(
            self,
            record: Record,
    ) -> RecordId:
        """Add an existing record, and return the record id."""
        self.check_record(record=record)
        self[record.rid] = record
        self.senders[record.sender].add(record.rid)
        return record.rid

    def check_record(
            self,
            record: Record,
    ) -> typing.List[str]:
        """Check if *record* is consistent.

        Args:
            record: record to be added to the store.

        Raises:
            RecordInconsistency: if record is not consistent with the store.

        """
        errs = []
        if record.sender not in self.users:
            errs.append(f'unknown sender uid: {record.sender}')

        unknown_receivers = [uid for uid in record.receivers
                             if uid not in self.users]
        if unknown_receivers:
            errs.append('unknown receivers uids: '
                        + ', '.join(str(rec) for rec in unknown_receivers))

        if errs:
            raise RecordInconsistency(', '.join(errs))

    # ---- CRUD methods ---------------------------------------------------
    def create(
            self,
            sender: UserIdLike,
            receivers: typing.List[UserIdLike],
            amount: AmountLike,
    ) -> RecordId:
        """Add a transation record.

        Args:
            sender: identifier of the sender
            receivers: sequence of the receivers identifiers. The send is
                not necessarily part of it.
            amount: monetary value exchanged in the transaction.

        Returns:
            created record identifier.

        Raises:
            ValueError or TypeError: if inputs are not compliant.

        """
        # check inputs
        errs = []
        try:
            sender_sf = UserId(sender)
        except (TypeError, ValueError):
            errs.append(f'cannot retrieve sender id from "{sender}"')

        if not receivers:
            errs.append('receivers undefined')
        try:
            receivers_sf = [UserId(uid) for uid in sorted(receivers)]
        except (TypeError, ValueError):
            errs.append('receivers shall be a list of integers')

        try:
            amount_sf = utils.fmt_amount(amount)
        except (ValueError, InvalidOperation):
            errs.append('amount shall be a number')

        if errs:
            raise RecordCreateError(', '.join(errs))

        # create the record
        record = Record(sender=sender_sf, receivers=receivers_sf,
                        amount=amount_sf)
        # and add it to the store
        try:
            return self.add(record)
        except RecordInconsistency as exc:
            raise RecordCreateError(f"inconsistent record: {exc}") from exc

    def update(
            self,
            rid: RecordIdLike,
            sender: UserIdLike = None,
            receivers: typing.Sequence[UserIdLike] = None,
            amount: AmountLike = None,
    ) -> Record:
        """Update a transaction record.

        Args:
            rid: identifier of the record, as returned by :py:meth:`create`.
            new_sender: if not `None`, new user identifier
            new_receivers: if not `None`, new sequence of receivers
                identifiers.
            new_amount: if not `None`, new amount.

        """
        rid_sf = RecordId(rid)
        # create a copy, that can be modified then checked before it is
        # used to overwrite the stored item
        record = copy.deepcopy(self[rid_sf])

        if sender is not None:
            old_sender = record.sender
            record.sender = UserId(sender)

        if receivers is not None and receivers:
            receivers_sf = []
            for receiver_sf in sorted(receivers):
                receivers_sf.append(receiver_sf)
            if receivers_sf != record.receivers:
                record.receivers = receivers_sf

        if amount is not None:
            # ensure two digits precision
            record.amount = utils.fmt_amount(amount)

        try:
            self.check_record(record=record)
        except RecordInconsistency as exc:
            raise RecordUpdateError(f"not consistent: {exc}") from exc
        else:
            if sender is not None:
                self.senders[old_sender].remove(rid_sf)
                self.senders[record.sender].add(rid_sf)
            self[rid_sf] = record
            return record

    def delete(
            self,
            rid: RecordIdLike,
    ) -> Record:
        """Delete a transaction record."""
        rid_sf = RecordId(rid)
        record = self[rid_sf]
        del self[rid_sf]
        return record

    # ---- magic methods --------------------------------------------------
    def __getitem__(
            self,
            rid: RecordIdLike,
    ) -> Record:
        """Return record corresponding to *rid*.

        Args:
            rid: record unique identifier.

        Raises:
            RecordAccessError: if there is no record associated to *rid*.

        """
        # convert *key* if necessary
        try:
            rid_sf = RecordId(rid)
        except ValueError as exc:
            raise RecordAccessError(f"cannot parse rid: {rid}") from exc
        # and get the record
        try:
            return super().__getitem__(rid_sf)
        except KeyError as exc:
            raise RecordAccessError(f"no record with rid={rid_sf}") \
                from exc


class Slug(str):
    """A string, usable as-is in URLs.

    >>> Slug('My new project')
    Slug(slug=my-new-project)
    >>>
    """

    def __new__(cls, name: str):
        """Create new Slug instance.

        Args:
            name: stringt to sluggify.

        """
        return str.__new__(cls, slugify(name))

    def __repr__(self) -> str:
        """Represent."""
        return f"Slug(slug={self})"


class SharesProject(dict):
    """A shared bills project, with its users and records.

    It's a wrapper for :py:class:`UserStore` and `RecordStore`.

    """

    def __init__(self, name: str, slug: Slug = None, json_data: dict = None):
        """Initialize."""
        super().__init__()
        self.name = name
        self.slug = slug
        self.users = UserStore(json_data=json_data)
        self.records = RecordStore(users=self.users, json_data=json_data)
        self['users'] = self.users
        self['records'] = self.records

    # ---- accounting functions -------------------------------------------
    def total(self, sender: UserIdLike = None) -> Decimal:
        """Sum up all transaction records, only for *sender* if provided."""
        if sender is None:
            record_seq = self.records.values()
        else:
            sender_sf: UserId = UserId(sender)
            record_seq = (self.records[rid]
                          for rid in self.records.senders[sender_sf])
        return utils.fmt_amount(sum(record.amount for record in record_seq))

    def users_balance(self) -> typing.Dict[UserId, Decimal]:
        """Compute the balance for each user.

        Value is positive if the user has paid more than received.

        """
        res = {uid: utils.fmt_amount(0) for uid in self.users.keys()}
        for record in self.records.values():
            # credit sender for the amount
            res[record.sender] += record.amount
            # compute receivers debits, and allocate them
            amount_share = utils.fmt_amount(record.amount
                                            / len(record.receivers))
            for receiver in record.receivers:
                res[receiver] -= amount_share
            # And the remainder is given as credit to last receiver
            remainder = record.amount - (amount_share * len(record.receivers))
            res[record.receivers[-1]] -= remainder
        return res

    def balance(self) -> typing.List[FlowType]:
        """Compute the project balance, as flows between users.

        Many solutions exist, here is the chosen policy:

        Loop over :py:func:`users_balance` output, and build two stacks with
        positive/negative balances. Then, for each user with positive balance,
        open a new flow for each next user with negative balance, up to
        completion. Users matching their balance are popped out of the stack.

        """
        res: typing.List[FlowType] = []
        pos_stack, neg_stack = [], []
        for uid, balance in self.users_balance().items():
            if balance > 0.0:
                pos_stack.append((uid, balance))
            elif balance < 0.0:
                neg_stack.append((uid, -1 * balance))  # positive figures only
            # and just ignore users with null balance
        for pos_user, pos_balance in pos_stack:
            while pos_balance > 10**-2:  # find a contributor
                # Pop next contributor. If the stack is empty, there's
                # a big problem in the balance!
                assert neg_stack, f"unbalance in users ({pos_balance} missing)"
                neg_user, neg_balance = neg_stack.pop(0)

                if neg_balance <= pos_balance:
                    res.append((neg_user, pos_user, neg_balance))
                    pos_balance -= neg_balance
                else:
                    res.append((neg_user, pos_user, pos_balance))
                    pos_balance = utils.fmt_amount(0)
                    # restack the remaining contribution
                    neg_stack.insert(0, (neg_user, neg_balance - pos_balance))
        return res

    # ---- content handling functions -------------------------------------
    def create_sample(self, nb_entries: int = 5):
        """Create some sample data."""
        self.users.create_sample(nb_entries=nb_entries)
        self.records.create_sample(nb_entries=nb_entries)

    def load(
            self,
            json_data: dict,
    ) -> str:
        """Load *json_data* (as dict) and add it to the store.

        Args:
            json_data: data from JSON file to be loaded.

        """
        msg = [
            self.users.load(json_data=json_data),
            self.records.load(json_data=json_data),
            ]
        return '; '.join(msg)

    def dump(self) -> dict:
        """Dump content to a dict ready for write in JSON format."""
        res = {}
        res.update(self.users.dump())
        res.update(self.records.dump())
        return res

    # ---- items CRUD methods wrappers ------------------------------------
    def users_as_list(self) -> typing.List[typing.Dict[str, typing.Any]]:
        """Return list of users, as dictionaries."""
        return [user.as_dict() for user in self.users.values()]

    def records_as_list(self) -> typing.List[typing.Dict[str, typing.Any]]:
        """Return list of records, as dictionaries."""
        return [record.as_dict() for record in self.records.values()]

    # ---- Users handling ----
    def create_user(
            self,
            name: typing.Text,
            email: typing.Text = None,
    ) -> UserId:
        """Create a new user by properties."""
        return self.users.create(name=name, email=email)

    def update_user(
            self,
            uid: UserIdLike,
            name: str = None,
            email: str = None,
    ) -> User:
        """Update a given user."""
        return self.users.update(uid=uid, name=name, email=email)

    def delete_user(
            self,
            uid: UserIdLike,
    ) -> User:
        """Delete a given user."""
        involved = collections.defaultdict(list)
        for rid, record in self.records.items():
            if record.sender == uid:
                involved[rid].append('sender')
            if any(receiver == uid for receiver in record.receivers):
                involved[rid].append('receiver')
        if involved:
            raise UserDeleteError(
                'involved '
                + ', '.join(f"in {rid} as {' and '.join(roles)}"
                            for rid, roles in involved.items()))
        return self.users.delete(uid=uid)

    # ---- records handling ----
    def create_record(
            self,
            sender: UserIdLike,
            receivers: typing.List[UserIdLike],
            amount: AmountLike,
    ) -> RecordId:
        """Add a transation record."""
        return self.records.create(
            sender=sender, receivers=receivers, amount=amount)

    def update_record(
            self,
            rid: RecordIdLike,
            sender: UserIdLike = None,
            receivers: typing.Sequence[UserIdLike] = None,
            amount: AmountLike = None,
    ) -> Record:
        """Update a transaction record."""
        return self.records.update(
            rid=rid, sender=sender, receivers=receivers, amount=amount)

    def delete_record(
            self,
            rid: RecordIdLike,
    ) -> Record:
        """Delete a transaction record."""
        return self.records.delete(rid=rid)

    # ---- magic methods and the like -------------------------------------
    def as_dict(self) -> typing.Dict[str, typing.Any]:
        """Export as a dictionary, JSON-compatible."""
        return {'slug': self.slug, 'name': self.name}


class Projects(dict):
    """Class to handle several SharesProject instances."""

    def __init__(self, json_data: dict = None):
        """Initialize."""
        super().__init__()
        #: follow-up of projects renamings
        self.renames: typing.Dict[str, str] = {}
        self.reversed_renames = collections.defaultdict(set)
        if json_data is not None:
            self.load(json_data=json_data)

    def load(
            self,
            json_data: dict,
    ) -> str:
        """Load *json_data* (as dict) and add it to the projects.

        Args:
            json_data: data from JSON file to be loaded.

        """
        prev_nb = len(self)
        for project_name, project_data in json_data['projects'].items():
            project = self.create_project(name=project_name)
            project.load(json_data=project_data)
        new_nb = len(self) - prev_nb
        mod_nb = len(self) - new_nb
        return f"projects: {new_nb} new, {mod_nb} mod"

    def dump(self) -> typing.Dict:
        """Export projects as dictionary, JSON-compatible."""
        return {"projects": {project.name: project.dump()
                             for project in self.values()}}

    def create_project(
            self,
            name: str,
    ) -> SharesProject:
        """Create a new project.

        Args:
            name: name of the project to create.

        Returns:
            the newly created project.

        """
        slug = Slug(name)
        if slug in self:
            raise ProjectCreateError(f"slug already used: {slug}")
        project = SharesProject(name=name, slug=slug)
        self[slug] = project
        return project

    def update_project(
            self,
            slug: Slug,
            new_name: str,
    ) -> Slug:
        """Update project *name*.

        Args:
            slug: slug of the project to update.
            new_name: new name to be used for the project.

        Returns:
            new slug of the updated project.

        """
        new_slug = Slug(new_name)
        if new_slug in self:
            raise ProjectUpdateError(f"slug already used: {new_slug}")
        # store the project at new slug
        self[new_slug] = self[slug]
        del self[slug]
        # update the project data
        self[new_slug].slug = new_slug
        self[new_slug].name = new_name
        # and maintain history
        # - forward history
        self.renames[slug] = new_slug
        for old_slug in self.reversed_renames[slug]:
            self.renames[old_slug] = new_slug
        # - backward history
        self.reversed_renames[new_slug] = self.reversed_renames[slug]
        del self.reversed_renames[slug]
        self.reversed_renames[new_slug].add(slug)
        return new_slug

    def delete_project(
            self,
            slug: Slug,
    ) -> SharesProject:
        """Delete project identified by its *slug*.

        Returns:
            the deleted project.

        """
        project = self.get_project(slug=slug)
        del self[slug]
        # and clean history of renames
        for old_slug in self.reversed_renames[slug]:
            del self.renames[old_slug]
        del self.reversed_renames[slug]
        return project

    def get_project(
            self,
            slug: Slug,
    ) -> SharesProject:
        """Get the project identified by its *slug*."""
        try:
            return self[slug]
        except KeyError:
            try:
                return self[self.renames[slug]]
            except KeyError as exc:
                raise ProjectAccessError(f"unknown slug: {exc}") from exc

    def projects_as_list(self) -> typing.List[typing.Dict[str, typing.Any]]:
        """Return of projects, as dictionaries."""
        return [project.as_dict() for _, project in sorted(self.items())]


#: type for transaction record value
UserIdLike = typing.Union[int, UserId]
RecordIdLike = typing.Union[str, uuid.UUID]
AmountLike = typing.Union[int, float, Decimal]
FlowType = typing.Tuple[UserId, UserId, Decimal]
SlugLike = typing.Union[str, Slug]
