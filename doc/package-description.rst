.. this file works as an entry point, and shall link to files describing
    each package.


Package Description
===================

Classes Diagram
---------------

.. mermaid::
  :align: center
  :caption: Overall Class Diagram (all in :py:mod:`districounts.models`)

  classDiagram

    %% relations between classes
    SharesProject "1" *-- "1" UserStore: .users
    RecordStore "1" *-- "1" UserStore: .users
    SharesProject "1" *-- "1" RecordStore: .records

    UserStore "1" *-- "*" User: by uid
    User "1" *-- "1" UserId: .uid

    RecordStore "1" *-- "*" Record: by rid
    Record "1" *-- "1" RecordId: .rid

    %% description of classes attributes
    class SharesProject {
      users: UserStore
      records: RecordStore

      load(json_data)
      dump()
      create_sample(nb_entries)

      create_user(name, email): UserId
      update_user(uid, name, email): User
      delete_user(uid): User
      users_as_list(): list[dict]

      create_record(sender, receivers, amount): RecordId
      update_record(rid, sender, receivers, amount): Record
      delete_record(rid): Record
      records_as_list(): list[dict]

      users_balance(): dict[UserId, Decimal]
      balance(): list[tuple[UserId, UserId, Decimal]]

    }

    class RecordStore {
      << dict >>

      senders: defaultdict[UserId, set[RecordId]]
      users: UserStore

      load(json_data)
      dump()
      create_sample(nb_entries)

      add(record)
      check_record(record)

      create()
      update()
      delete()
    }

    class Record {
      rid: RecordId
      sender: Userid
      receivers: list(UserId)
      amount: Amount
      currency: str

      from_dict(cls, data)
      as_dict()
    }

    class RecordId {
      << uuid.UUID >>
    }

    class UserStore {
      << dict >>

      by_name: defaultdict[str, set[UserId]]

      load(json_data)
      dump()
      create_sample(nb_entries)

      add(user)

      create()
      update()
      delete()
    }

    class User {
      uid: UserId
      name: str
      email: str

      from_dict(cls, data)
      as_dict()
    }

    class UserId {
      << int >>
    }


Description of Each Module
--------------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   __init__
   cli
   models
   backend
