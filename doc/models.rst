models.py
===========

Classes structure
-----------------

.. inheritance-diagram:: districounts.models

Members description
-------------------

.. automodule:: districounts.models
    :members:
    :undoc-members:
    :show-inheritance:
