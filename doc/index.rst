.. districounts documentation master file, created by
   sphinx-quickstart on Sun Nov  3 12:38:34 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to districounts' documentation!
=======================================

``discricounts`` is a toy project, aimed at developping my coding abilities.
To describe it quickly, it is a clone of Tricount_.

For instruction about basic usage, have a look at the :doc:`readme`.

``districounts`` is in version |release|. Detailed description of the
functionalities per released version are all on the `tags page`_ under
the `framagit project page`_.

.. _Tricount: www.tricount.com
.. _framagit project page: https://framagit.org/ojob/districounts/
.. _tags page: https://framagit.org/ojob/districounts/-/tags
.. _milestones: https://framagit.org/ojob/districounts/-/milestones

Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   tutorial
   package-description
   contributing
   authors
   license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
