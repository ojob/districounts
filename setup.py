# coding: utf-8
"""Configuration for python package managers."""

# ---- built-in import ----------------------------------------------------
import pathlib

# ---- third-party imports ------------------------------------------------
import setuptools

# ---- constants definition -----------------------------------------------
HERE = pathlib.Path(__file__).parent
VERSION_FN = HERE / 'src' / 'districounts' / 'VERSION'
README_FN = pathlib.Path('README.rst')


# ---- setup configuration ------------------------------------------------
setuptools.setup(
    name="districounts",
    version=VERSION_FN.open().read().strip(),

    author="Joël Bourgault",
    author_email="joel.bourgault@gmail.com",

    description="Hobby project for multi-users countability",
    long_description=README_FN.open().read(),
    long_description_content_type="text/markdown",
    url="https://framagit.org/ojob/districounts",

    packages=setuptools.find_packages('src'),
    package_dir={'': 'src'},

    classifiers=[
        "Programming Language :: Python :: 3",
        ("License :: OSI Approved :: GNU General Public License v3 or later "
         "(GPLv3+)"),
        "Operating System :: OS Independent",
    ],

    python_requires='>=3.6',
    install_requires=[
        'bottle',
        'click',
        'python-slugify',
    ],

    # define package data in MANIFEST.in
    include_package_data=True,

    # test_suite='districounts.tests',
    entry_points=dict(
        console_scripts=[
            'districounts=districounts.cli:cli',
        ],
    ),
)
