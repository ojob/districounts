# coding: utf-8
"""This module provides testing of the Command-Line Interface."""

# ---- built-in imports ---------------------------------------------------

# ---- third-party imports ------------------------------------------------
from click.testing import CliRunner

# ---- local imports ------------------------------------------------------
from districounts import __version__
from districounts.cli import cli


# ---- functions definition -----------------------------------------------
def test_cli():
    """Test CLI."""
    runner = CliRunner()

    # check call with no parameters
    result = runner.invoke(cli)
    assert result.exit_code == 0

    # check version option usage
    result = runner.invoke(cli, ['--version'])
    assert result.exit_code == 0
    # check that version is the last output stuff
    assert result.output.split(', ')[-1].strip() == f"version {__version__}"
