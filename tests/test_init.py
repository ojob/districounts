# coding: utf-8
"""Tests for main module."""

import districounts


def test_version():
    """Test version parsing."""
    version_parts = districounts.__version__.split('.')
    assert len(version_parts) >= 3
    assert all(part == str(int(part))
               for part in version_parts[:3])
